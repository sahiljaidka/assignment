var async = require('async');
var mongoose = require('mongoose');
var Models = require('../Models');
var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
var Services = require('../Services');
var ERROR = Config.AppConstants.STATUS_MSG.ERROR;

var adminRegister = function (payload, callb) {
    var adminDoesNotExist = false;
    var accessToken = null;
    var dataToSave = payload;
    var adminData = null;
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    payload.email = payload.email.toLowerCase();
    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                //$or: [{email: payload.email}, {phoneNumber: payload.phoneNumber}]
                email: payload.email
            };
            Services.AdminService.getAdmin(criteria, {}, {lean: true}, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.ADMIN_ALREADY_REGISTERED)
                    } else {
                        adminDoesNotExist = true;
                        cb()
                    }
                }
            })
        },
        function (cb) {
            //Insert Into DB
            dataToSave.phoneNumber = payload.phoneNumber;
            dataToSave.createdAt = new Date().toISOString();
            Services.AdminService.createAdmin(dataToSave, function (err, adminDataFromDB) {
                //Models.Customers(dataToSave).save(function (err, customerDataFromDB) {
                console.log('hello', err, adminDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('admin.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('admin.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    adminData = adminDataFromDB;
                    cb();
                }
            });
        }, function (cb) {
            //Set Access Token
            if (adminData) {
                var tokenData = {
                    id: adminData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.ADMIN
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callb(err)
        } else {
            var details = {
                id: adminData._id,
                adminDetails: dataToSave,
                accessToken: accessToken
            };
            return callb(null, details);
        }
    })
};

var adminLogin = function (payload, callBackRoute) {
    var userFound = false;
    var successLogin = false;
    var accessToken = null;
    var returnedData = {};
    var updatedUserDetails = null;

    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(payload.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                //$or: [{email: payload.emailOrPhone}, {mobileNumber: payload.emailOrPhone}]
                email: payload.email
            };
            var option = {
                lean: true
            };
            Services.AdminService.getAdmin(criteria, {}, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    console.log("=============",result[0]);
                    if(result[0].role == payload.role) {
                        //console.log("=============",userFound.role);
                        userFound = result && result[0] || null;
                        returnedData = result;
                        cb();
                    }
                    else{
                        cb(ERROR.NOT_REGISTERED_ROLE);
                    }
                }
            });
        },
        function (cb) {
            //validations
            if (!userFound) {
                //cb("You are not registered with us.");
                cb(ERROR.EMAIL_NOT_FOUND)
            } else {
                if (userFound && userFound.password != UniversalFunctions.CryptData(payload.password)) {
                    //cb("Incorrect Password");
                    cb(ERROR.INCORRECT_PASSWORD)
                } else {
                    successLogin = true;
                    cb();
                }
            }
        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.ADMIN
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            //cb("Error 2")
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);
                //cb("Error 3")
            }

        }
    ], function (err, data) {
        if (err) {
            callBackRoute(err)
        } else {
            var userDetails = UniversalFunctions.deleteUnnecessaryUserData(userFound);
            var details = {
                adminDetails: userDetails,
                accessToken: accessToken
            };
            callBackRoute(null, details);
        }
    })
};

var getProfile = function (data, callbackRoute) {
    console.log("iddddddddddddddddddd", data.id);
    var query = {
        _id: data.id
    };
    var projection = {
        fullName: 1,
        email: 1,
        phoneNumber: 1,
        role: 1,
        createdAt: 1
    };
    var options = {lean: true};
    Services.AdminService.getAdmin(query, projection, options, function (err, data) {
        if (err) {
            callbackRoute(err);
        } else {
            var adminData = data && data[0] || null;
            console.log("adminData-------->>>" + JSON.stringify(adminData));
            if (adminData == null) {
                callbackRoute(ERROR.INVALID_TOKEN);
                //callbackRoute("Invalid Token");
            } else {
                callbackRoute(null, adminData);
            }
        }
    });
};

var changePassword = function (payload, tokenData, callbackRoute) {
    var oldPassword = UniversalFunctions.CryptData(payload.oldPassword);
    var newPassword = UniversalFunctions.CryptData(payload.newPassword);
    async.series([
            function (callback) {
                var query = {
                    _id: tokenData.id
                };
                var projection = {
                    password: 1
                };
                var options = {lean: true};
                Services.AdminService.getAdmin(query, projection, options, function (err, data) {
                    if (err) {
                        callback(err);
                    } else {
                        var customerData = data && data[0] || null;
                        console.log("customerData-------->>>" + JSON.stringify(customerData));
                        if (customerData == null) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            if (data[0].password == oldPassword && data[0].password != newPassword) {
                                callback(null);
                            }
                            else if (data[0].password != oldPassword) {
                                callback(ERROR.WRONG_PASSWORD)
                            }
                            else if (data[0].password == newPassword) {
                                callback(ERROR.NOT_UPDATE)
                            }
                        }
                    }
                });
            },
            function (callback) {
                var dataToUpdate = {$set: {'password': UniversalFunctions.CryptData(payload.newPassword)}};
                var condition = {_id: tokenData.id};
                Services.AdminService.updateAdmin(condition, dataToUpdate, {}, function (err, user) {
                    console.log("customerData-------->>>" + JSON.stringify(user));
                    if (err) {
                        callback(err);
                    } else {
                        if (!user || user.length == 0) {
                            callback(ERROR.NOT_FOUND);
                        }
                        else {
                            callback(null);
                        }
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var getAllCustomers = function (callbackRoute) {
    var customerList = {};
    async.series([
            function (callback) {
                var projection = {
                    firstName: 1,
                    lastName: 1,
                    email: 1,
                    mobileNumber: 1,
                    gender: 1,
                    myStatus: 1
                };
                var setOptions = {
                    lean: true
                };
                Models.Customers.find({}, projection, setOptions, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        customerList = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null, {'customerList': customerList});
            }
        });
};

var searchCustomer = function (queryData, callbackRoute) {
    var customerList = {};
    async.series([
            function (callback) {
                if (queryData == null) {
                    callback();
                }
                else {
                    var userDetails = {
                        queryString: queryData
                    };
                    var projection = {
                        firstName: 1,
                        email: 1,
                        _id: 1
                    };
                    var conditions = {"$text": {"$search": queryData}};
                    Models.Customers.find(conditions, projection, {lean: true}, function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            customerList = result;
                            callback();
                        }
                    });
                }
            }
        ],
        function (error, results) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null, {'customerList': customerList});
                //return callbackRoute(results);
            }
        });
};

module.exports = {
    adminRegister: adminRegister,
    adminLogin: adminLogin,
    getProfile: getProfile,
    changePassword: changePassword,
    getAllCustomer: getAllCustomers,
    searchCustomer: searchCustomer
};