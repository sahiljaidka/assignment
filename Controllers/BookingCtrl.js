/**
 * Created by SONY on 4/14/2016.
 */
var async = require('async');
var mongoose = require('mongoose');
var Models = require('../Models');
var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
var Services = require('../Services');
var ERROR = Config.AppConstants.STATUS_MSG.ERROR;

var createBooking = function (payload, tokenData, callback) {
    var dataToSet = {};
    var booking;
    async.series([
            function (cb) {
                dataToSet = {
                    customerId: tokenData.id
                };
                Services.BookingService.createBooking(dataToSet, function (err, data) {
                    if (!err) {
                        booking = data;
                        cb(null);
                    }
                    else {
                        cb(err);
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                callback(error);
            } else {
                callback(null, {booking: booking});
            }
        });
};

module.exports = {
    createBooking: createBooking
};