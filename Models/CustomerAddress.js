/**
 * Created by SONY on 4/3/2016.
 */

var mongoose = require('mongoose');
var Config = require('../Config');
var Schema = mongoose.Schema;

var customerAddresses = new Schema({
    customerId: {type: Schema.ObjectId, ref: 'Customers'},
    customerLocation: {
        'type': {type: String, enum: "Point", default: "Point"},
        coordinates: {type: [Number], default: [0, 0]}
    },
    streetAddress: {type: String},
    city: {type: String},
    apartmentNumber: {type: String},
    state: {type: String},
    country: {type: String},
    postalCode: {type: String},
    isDeleted: {type: Boolean, default:false}
});

module.exports = mongoose.model('customerAddresses', customerAddresses);