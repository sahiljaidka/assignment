/**
 * Created by SONY on 4/9/2016.
 */

var mongoose = require('mongoose');
var Config = require('../Config');
var Schema = mongoose.Schema;
var booking = Config.AppConstants.DATABASE.BOOKING_STATUS;

var Booking = new Schema({
    customerId: {type: [Schema.ObjectId], ref: 'Customers'},
    adminId: {type: [Schema.ObjectId], ref: 'Admin'},
    bookingStatus: {
        type: String, enum: [
            booking.PENDING,
            booking.BOOKED,
            booking.STARTED,
            booking.COMPLETED,
            booking.CANCELLED
        ], default: booking.PENDING
    },
    bookingDuration: {type: Number},
    bidPrice: {type: Number},
    startTime: {type: Date},
    endTime: {type: Date}
});

module.exports = mongoose.model('Booking', Booking);