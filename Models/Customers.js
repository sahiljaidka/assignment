/**
 * Created by SONY on 3/12/2016.
 */

var mongoose = require('mongoose');
var Config = require('../Config');
var Schema = mongoose.Schema;
var device = Config.AppConstants.DATABASE.DEVICETYPE;
var gender = Config.AppConstants.DATABASE.GENDER;

var Status = new Schema({
    statusText: {type: String, required: true},
    addedAt: {type: Date, default: Date.now},
    //likes: {type: [Schema.ObjectId], unique: true, default: [], ref: 'Customers'}
});

var Customers = new Schema({
    firstName: {type: String},
    lastName: {type: String},
    email: {type: String},
    password: {type: String},
    countryCode: {type: String},
    mobileNumber: {type: String},
    deviceToken: {type: String},
    accessToken: {type: String},
    deviceType: {
        type: String, enum: [
            device.ANDROID,
            device.IOS
        ]
    },
    gender: {
        type: String, enum: [
            gender.FEMALE,
            gender.MALE
        ]
    },
    //statusId: {type: Schema.ObjectId, ref: 'Status'}
    myStatus: {type: [Status], default: []},
    OTPCode: {type: String, trim: true},
    isPhoneVerified: {type: Boolean, default: false},
    facebookId: {type: String}
    //CustomerAddress : [{type: Schema.ObjectId, ref: 'customerAddresses'}]
    //status: [{type: String}]
});
Customers.index({firstName: "text"});
module.exports = mongoose.model('Customers', Customers);