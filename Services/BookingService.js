/**
 * Created by SONY on 4/14/2016.
 */
'use strict';

var Models = require('../Models');


var updateBooking = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.Booking.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createBooking = function (objToSave, callback) {
    new Models.Booking(objToSave).save(callback)
};
//Delete User in DB
var deleteBooking = function (criteria, callback) {
    Models.Booking.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getBooking = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.Booking.find(criteria, projection, options, callback);
};
var getAllBookings = function (criteria, projection, populate, sortOptions, setOptions, callback) {
    console.log("dao........", criteria, projection, populate)
    Models.Booking.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
        callback(err, result);
    });
};
module.exports = {
    createBooking: createBooking,
    deleteBooking: deleteBooking,
    getBooking: getBooking,
    updateBooking: updateBooking,
    getAllBookings: getAllBookings


};

