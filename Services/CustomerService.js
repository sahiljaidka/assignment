/**
 * Created by SONY on 3/24/2016.
 */
'use strict';

var Models = require('../Models');

var updateCustomer = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.Customers.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createCustomer = function (objToSave, callback) {
    new Models.Customers(objToSave).save(callback)
};
//Delete User in DB
var deleteCustomer = function (criteria, callback) {
    Models.Customers.remove(criteria, callback);
};

//Get Users from DB
var getCustomer = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.Customers.find(criteria, projection, options, callback);
};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode: {$ne: null}
    };
    var projection = {
        OTPCode: 1
    };
    var options = {
        lean: true
    };
    Models.Customers.find(criteria, projection, options, function (err, dataAry) {
        if (err) {
            callback(err)
        } else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0) {
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null, generatedCodes);
        }
    })
};

module.exports = {
    updateCustomer: updateCustomer,
    createCustomer: createCustomer,
    deleteCustomer: deleteCustomer,
    getCustomer: getCustomer,
    getAllGeneratedCodes:getAllGeneratedCodes
};
