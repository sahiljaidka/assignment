/**
 * Created by clicklabs113 on 3/10/16.
 */
module.exports = [
    {register: require('./swagger')},
    {register: require('./good-console')}
    , {register: require('./auth-token')}
];