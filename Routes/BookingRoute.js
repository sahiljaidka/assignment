/**
 * Created by SONY on 4/14/2016.
 */
'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Config = require('../Config');

var createBooking = {
    method: 'POST',
    path: '/api/booking/customerCreate',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var payload = request.payload;
        Controllers.BookingCtrl.createBooking(payload, userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Customer create booking',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                startTime: Joi.date().required().description('in ISO Format'),
                endTime: Joi.date().required().description('in ISO Format'),
                bookingDuration: Joi.number().required(),
                bidPrice: Joi.number().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

module.exports = [
    createBooking
];