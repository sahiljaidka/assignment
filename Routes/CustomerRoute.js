/**
 * Created by SONY on 3/13/2016.
 */
'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Config = require('../Config');

var customerRegister = {
    method: 'POST',
    path: '/api/customer/register',
    handler: function (request, reply) {
        var payload = request.payload;
        Controllers.CustomerController.customerRegister(payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
                //reply(null, data);
            }
        });
    },
    config: {
        validate: {
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                lastName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                password: Joi.string().required().min(6),
                countryCode: Joi.string().required(),
                mobileNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid([Config.AppConstants.DATABASE.DEVICETYPE.IOS, Config.AppConstants.DATABASE.DEVICETYPE.ANDROID]),
                gender: Joi.string().required().valid([Config.AppConstants.DATABASE.GENDER.MALE, Config.AppConstants.DATABASE.GENDER.FEMALE])

            },
            failAction: UniversalFunctions.failActionFunction
        },
        description: 'Register Customer',
        tags: ['api', 'Customer'],
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var customerLogin = {
    method: 'POST',
    path: '/api/customer/login',
    handler: function (request, reply) {
        var payload = request.payload;
        console.log(request.payload.email);
        Controllers.CustomerController.customerLogin(payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Login for Customer',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                emailOrPhone: Joi.string().required(),
                password: Joi.string().required().min(6)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var getProfile = {
    method: 'GET',
    path: '/api/customer/getProfile',
    config: {
        description: 'Get profile of Customer',
        auth: 'UserAuth',
        tags: ['api', 'customer'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            console.log("-----route------>>" + JSON.stringify(userData));
            if (userData && userData.id) {
                Controllers.CustomerController.getProfile(userData, function (err, success) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                        //reply(err);
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, success));
                        //reply(userData);
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(Config.AppConstants.STATUS_MSG.ERROR.INVALID_TOKEN));
                //reply("Invalid Token");
            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var changePassword = {
    method: 'PUT',
    path: '/api/customer/changePassword',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerController.changePassword(request.payload, userData, function (err, data) {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.PASSWORD_RESET, data));
                //return reply("Password Changed Successfully", data);
            }
            else {
                reply(UniversalFunctions.sendError(err));
                //return reply(err);
            }
        });
    },
    config: {
        description: 'Change Password',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(6),
                newPassword: Joi.string().required().min(6)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var addStatus = {
    method: 'POST',
    path: '/api/customer/addStatus',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var payload = request.payload;
        Controllers.StatusController.addStatus(payload, userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.LOGOUT, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Add Status of Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                status: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var customerLogout = {
    method: 'PUT',
    path: '/api/customer/logout',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerController.customerLogout(userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Customer Logout',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

//var addStatus = {
//    method: 'POST',
//    path: '/api/customer/addStatus',
//    handler: function (request, reply) {
//        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
//        var payload = request.payload;
//        Controllers.StatusController.addStatus(payload, userData, function (err, data) {
//            if (err) {
//                reply(UniversalFunctions.sendError(err));
//                //reply(err);
//            } else {
//                reply(UniversalFunctions.sendSuccess(null, data));
//                //reply(null, data);
//            }
//        });
//    },
//    config: {
//        description: 'Add Status of Customer',
//        tags: ['api', 'customer'],
//        auth: 'UserAuth',
//        validate: {
//            headers: UniversalFunctions.authorizationHeaderObj,
//            payload: {
//                status: Joi.string().required()
//            },
//            failAction: UniversalFunctions.failActionFunction
//        },
//        plugins: {
//            'hapi-swagger': {
//                responseMessages: swaggerDefaultResponseMessages
//            }
//        }
//    }
//};

var updateStatus = {
    method: 'PUT',
    path: '/api/customer/updateStatus',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var payload = request.payload;
        Controllers.StatusController.updateStatus(payload, userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Update Customer Status',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                statusId: Joi.string().required(),
                status: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var verifyOTP = {
    method: 'PUT',
    path: '/api/customer/verifyOTP',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerController.verifyOTP(request.payload, userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.VERIFY_COMPLETE, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Verify OTP for Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                OTPCode: Joi.string().length(4).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var resendOTP = {
    method: 'PUT',
    path: '/api/customer/resendOTP',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerController.resendOTP(userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Resend OTP for Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var addAddress = {
    method: 'POST',
    path: '/api/customer/addAddress',
    handler: function (request, reply) {

        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        if (!UniversalFunctions.validateLatLongValues(request.payload.latitude, request.payload.longitude)) {
            reply(UniversalFunctions.sendError(Config.AppConstants.STATUS_MSG.ERROR.INVALID_LAT));
        }
        else {
            Controllers.CustomerController.addAddress(request.payload, userData, function (err, user) {
                if (!err) {
                    reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.ADDRESS_CREATE, user));
                }
                else {
                    reply(UniversalFunctions.sendError(err));
                }
            });
        }

    },
    config: {
        description: 'Add Address for Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                streetAddress: Joi.string().required(),
                city: Joi.string().required(),
                apartmentNumber: Joi.number().required(),
                postalCode: Joi.string().required(),
                country: Joi.string().required(),
                state: Joi.string().required(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var getAddress = {
    method: 'GET',
    path: '/api/customer/getAddress',
    config: {
        description: 'Get address of Customer',
        auth: 'UserAuth',
        tags: ['api', 'customer'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            console.log("-----route------>>" + JSON.stringify(userData));
            if (userData && userData.id) {
                Controllers.CustomerController.getAddress(userData, function (err, success) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                        //reply(err);
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, success));
                        //reply(userData);
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(Config.AppConstants.STATUS_MSG.ERROR.INVALID_TOKEN));
                //reply("Invalid Token");
            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var deleteAddress = {
    method: 'DELETE',
    path: '/api/customer/deleteAddress',
    config: {
        description: 'Delete address of Customer',
        auth: 'UserAuth',
        tags: ['api', 'customer'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            console.log("-----route------>>" + JSON.stringify(userData));
            if (userData && userData.id) {
                Controllers.CustomerController.deleteAddress(request.payload, userData, function (err, success) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                        //reply(err);
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, success));
                        //reply(userData);
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(Config.AppConstants.STATUS_MSG.ERROR.INVALID_TOKEN));
                //reply("Invalid Token");
            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                addressId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var registerViaFacebook = {
    method: 'POST',
    path: '/api/customer/registerViaFacebook',
    handler: function (request, reply) {
        Controllers.CustomerController.registerViaFacebook(request.payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
                //reply(null, data);
            }
        });
    },
    config: {
        validate: {
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                lastName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                facebookId: Joi.string().required(),
                countryCode: Joi.string().required(),
                mobileNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid([Config.AppConstants.DATABASE.DEVICETYPE.IOS, Config.AppConstants.DATABASE.DEVICETYPE.ANDROID]),

            },
            failAction: UniversalFunctions.failActionFunction
        },
        description: 'Register Customer Via Facebook',
        tags: ['api', 'Customer'],
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var loginViaFacebook = {
    method: 'POST',
    path: '/api/customer/loginViaFacebook',
    handler: function (request, reply) {
        Controllers.CustomerController.loginViaFacebook(request.payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
            }
        });
    },
    config: {
        validate: {
            payload: {
                facebookId: Joi.string().required(),
                email: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        description: 'Login Customer Via Facebook',
        tags: ['api', 'Customer'],
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var createBooking = {
    method: 'POST',
    path: '/api/customer/createBooking',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerController.addAddress(request.payload, userData, function (err, user) {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.ADDRESS_CREATE, user));
            }
            else {
                reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Create Booking for Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                adminId: Joi.string().required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

module.exports = [
    customerRegister,
    customerLogin,
    getProfile,
    changePassword,
    addStatus,
    customerLogout,
    updateStatus,
    verifyOTP,
    resendOTP,
    addAddress,
    getAddress,
    deleteAddress,
    registerViaFacebook,
    loginViaFacebook
];